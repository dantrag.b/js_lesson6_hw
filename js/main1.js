///////////////////////////////////// Задание 1 /////////////////////////////////////

// Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище),
// rate (ставка за день роботи), days (кількість відпрацьованих днів).
// Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
// Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.


function Worker(name, surname, rate, days) {
  this.name = name;
  this.surname = surname;
  this.rate = rate;
  this.days = days;
  this.getSalary = function () {
    return document.write(`Заработная плата ${surname} ${name} за ${days} рабочих дней, составляет ${rate * days} грн. <br>`);
  }
}

const orest = new Worker('Орест', 'Парасюк', 2000, 24);
const elena = new Worker('Елена', 'Иванова', 1500, 63);

orest.getSalary();
elena.getSalary();

///////////////////////////////////// Задание 2 /////////////////////////////////////

// Реалізуйте клас MyString, який матиме такі методи: метод reverse(),
// який параметром приймає рядок, а повертає її в перевернутому вигляді, метод ucFirst(),
// який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою
// та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.

function MyString(str) {

  this.reverse = function (str) {
    if ('string' !== (typeof str)) { return 'Это не строка'; }
    return str.split('').reverse().join('');
  }

  this.ucFirst = function (str) {
    if ('string' !== (typeof str)) { return 'Это не строка'; }
    return (str.slice(0, 1).toUpperCase() + str.slice(1));
  }

  this.ucWords = function (str) {
    if ('string' !== (typeof str)) { return 'Это не строка'; }
    str = str.split('');
    str[0] = str[0].toUpperCase();
    for (let i = 0; i < str.length; i++) {
      if (str[i] == ' ') { str[i + 1] = str[i + 1].toUpperCase(); }
    }
    return str.join('');
  }
}

const test = new MyString();

const str1 = 'Слава Украї́ні! Героям слава!';
const str2 = 'слава украї́ні! героям слава!';
const str3 = 4;


console.log(test.reverse(str1));
console.log(test.ucFirst(str2));
console.log(test.ucWords(str2));

console.log(test.ucWords(str3));